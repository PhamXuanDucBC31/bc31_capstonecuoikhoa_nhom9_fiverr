import "./App.css";
import HomePage from "./Page/HomePage/HomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Page/LoginPage/LoginPage";
import ListTaskPage from "./Page/ListTaskPage/ListTaskPage";
import DetailTaskPage from "./Page/DetailTaskPage/DetailTaskPage";
import Layout from "./HOC/Layout";
import TypeOfTaskPage from "./Page/TypeOfTaskPage/TypeOfTaskPage";
import UserInforPage from "./Page/UserInforPage/UserInforPage";
import RegisterPage from "./Page/RegisterPage/RegisterPage";
import AdminLayout from "./HOC/AdminLayout";
import UserManagement from "./Page/Admin/UserManagement/UserManagement.jsx";
import EditUser from "./Page/Admin/UserManagement/EditUser/EditUser";
import TaskManagement from "./Page/Admin/TaskManagement/TaskManagement";
import TaskTypeManagement from "./Page/Admin/TaskTypeManagement/TaskTypeManagement";
import ServiceManagement from "./Page/Admin/ServiceManagement/ServiceManagement";
import AddAdmin from "./Page/Admin/UserManagement/AddAdmin/AddAdmin";
import AddTask from "./Page/Admin/TaskManagement/AddTask/AddTask.jsx";
import AddTaskType from "./Page/Admin/TaskTypeManagement/AddTaskType/AddTaskType";
import EditTaskType from "./Page/Admin/TaskTypeManagement/EditTaskType/EditTaskType";
import EditTask from "./Page/Admin/TaskManagement/EditTask/EditTask";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {/* --------------------------------------------USER------------------------------------------------- */}
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/taskList/:tenCongViec"
            element={<Layout Component={ListTaskPage} />}
          />
          <Route
            path="/taskList/categories/:maChiTietLoai"
            element={<Layout Component={ListTaskPage} />}
          />
          <Route
            path="/typeOfTask/:maLoaiCongViec"
            element={<Layout Component={TypeOfTaskPage} />}
          />

          <Route
            path="/detailTask/:id"
            element={<Layout Component={DetailTaskPage} />}
          />
          <Route path="/user" element={<Layout Component={UserInforPage} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          {/* --------------------------------------------ADMIN------------------------------------------------- */}

          {/* --------------------------------------------ADMIN/user------------------------------------------------- */}
          <Route
            path="/admin"
            element={<AdminLayout Component={UserManagement} />}
          />

          <Route
            path="/admin/editUser/:id"
            element={<AdminLayout Component={EditUser} />}
          />

          <Route
            path="/admin/addAdmin"
            element={<AdminLayout Component={AddAdmin} />}
          />
          {/* --------------------------------------------ADMIN/task------------------------------------------------- */}
          <Route
            path="/admin/task"
            element={<AdminLayout Component={TaskManagement} />}
          />
          <Route
            path="/admin/addTask"
            element={<AdminLayout Component={AddTask} />}
          />
          <Route
            path="/admin/editTask/:id"
            element={<AdminLayout Component={EditTask} />}
          />
          {/* --------------------------------------------ADMIN/taskType------------------------------------------------- */}
          <Route
            path="/admin/taskType"
            element={<AdminLayout Component={TaskTypeManagement} />}
          />
          <Route
            path="/admin/addTaskType"
            element={<AdminLayout Component={AddTaskType} />}
          />
          <Route
            path="/admin/editTaskType/:id"
            element={<AdminLayout Component={EditTaskType} />}
          />
          {/* --------------------------------------------ADMIN/service------------------------------------------------- */}
          <Route
            path="/admin/service"
            element={<AdminLayout Component={ServiceManagement} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
