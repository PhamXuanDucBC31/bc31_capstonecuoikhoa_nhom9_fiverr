import { Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginAction } from "../../redux/Actions/userAction";
import { localStorageServ } from "../../Service/localStorageService";
import { userService } from "../../Service/userService";

export default function LoginPage() {
  let dispatch = useDispatch();
  let history = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        dispatch(loginAction(res.data.content));
        localStorageServ.user.set(res.data.content);
        setTimeout(() => {
          history(-1);
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="bg-green-500 w-screen h-screen p-10">
      <div className="container rounded-xl mx-auto p-10 bg-white flex ">
        <div className="w-1/2 h-96 "></div>
        <div className="w-1/2">
          <Form
            name="basic"
            layout="vertical"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label={<p className="text-blue-500 font-medium">Email</p>}
              name="email"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập email!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label={<p className="text-blue-500 font-medium">Mật khẩu</p>}
              name="password"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <div className="flex justify-center gap-5">
              <button className="rounded px-5 py-2 text-white bg-red-500 hover:opacity-80">
                Đăng nhập {""}
              </button>
              <button
                className="rounded px-5 py-2 text-white bg-blue-500 hover:opacity-80"
                onClick={() => {
                  window.location.href = "/register";
                }}
              >
                Đăng kí {""}
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
