import React from "react";
import { Form, Input } from "antd";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import { postAddTaskAction } from "../../../../redux/Actions/taskAction";

export default function AddTask() {
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      tenCongViec: "",
      danhGia: 0,
      giaTien: 0,
      nguoiTao: 0,
      hinhAnh: "",
      moTa: "",
      maChiTietLoaiCongViec: 0,
      moTaNgan: "",
      saoCongViec: 0,
    },
    onSubmit: (values) => {
      console.log(values);

      dispatch(postAddTaskAction(values));
    },
  });

  return (
    <div className="container">
      <h3 className="text-4xl mb-5">Thêm Công việc</h3>

      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
      >
        <Form.Item label="Tên công việc" onChange={formik.handleChange}>
          <Input value={formik.values.tenCongViec} name="tenCongViec" />
        </Form.Item>

        <Form.Item label="Đánh giá" onChange={formik.handleChange}>
          <Input type="number" value={formik.values.danhGia} name="danhGia" />
        </Form.Item>

        <Form.Item label="Giá tiền" onChange={formik.handleChange}>
          <Input type="number" value={formik.values.giaTien} name="giaTien" />
        </Form.Item>

        <Form.Item label="Người tạo" onChange={formik.handleChange}>
          <Input type="number" value={formik.values.nguoiTao} name="nguoiTao" />
        </Form.Item>

        <Form.Item label="Hình ảnh" onChange={formik.handleChange}>
          <Input value={formik.values.hinhAnh} name="hinhAnh" />
        </Form.Item>

        <Form.Item label="Mô tả" onChange={formik.handleChange}>
          <Input value={formik.values.moTa} name="moTa" />
        </Form.Item>

        <Form.Item
          label="Mã chi tiết loại công việc"
          onChange={formik.handleChange}
        >
          <Input
            type="number"
            value={formik.values.maChiTietLoaiCongViec}
            name="maChiTietLoaiCongViec"
          />
        </Form.Item>

        <Form.Item label="Mô tả ngắn" onChange={formik.handleChange}>
          <Input value={formik.values.moTaNgan} name="moTaNgan" />
        </Form.Item>

        <Form.Item label="Sao công việc" onChange={formik.handleChange}>
          <Input
            type="number"
            value={formik.values.saoCongViec}
            name="saoCongViec"
          />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button
            type="submit"
            className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
          >
            THÊM CÔNG VIỆC
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
