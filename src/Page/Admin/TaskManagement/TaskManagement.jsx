import React, { Fragment, useEffect } from "react";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Input, Table } from "antd";
import {
  deleteTaskByIdAction,
  gettaskListAction,
} from "../../../redux/Actions/taskAction";

export default function TaskManagement() {
  const dispatch = useDispatch();
  const { Search } = Input;
  const { arrTaskList } = useSelector((state) => state.taskReducer);
  useEffect(() => {
    dispatch(gettaskListAction());
  }, []);

  const data = arrTaskList;

  const columns = [
    // render tài khoản
    {
      title: "id",
      dataIndex: "id",
      width: "2%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend", "ascend"],
    },

    // render họ tên
    {
      title: "Tên công việc",
      dataIndex: "tenCongViec",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => {
        let tenCongViecA = a.tenCongViec.toLowerCase().trim();
        let tenCongViecB = b.tenCongViec.toLowerCase().trim();
        if (tenCongViecA > tenCongViecB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    //render email
    {
      title: "Giá tiền",
      dataIndex: "giaTien",
      width: "3%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => a.giaTien - b.giaTien,
      sortDirections: ["descend", "ascend"],
    },

    //rener role người dùng
    {
      title: "Mô tả",
      dataIndex: "moTa",
      width: "20%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sortDirections: ["descend", "ascend"],
    },

    //render thao tác
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      width: "10%",

      sortDirections: ["descend", "ascend"],
      render: (text, arrTaskList, index) => {
        return (
          <Fragment>
            <NavLink
              to={`/admin/editTask/${arrTaskList.id}`}
              key={1}
              className="mr-2 text-2xl"
            >
              <EditOutlined style={{ color: "blue" }} />
            </NavLink>

            <NavLink
              key={2}
              className="ml-2 text-2xl"
              onClick={() => {
                deleteUser(arrTaskList.id);
              }}
            >
              <DeleteOutlined style={{ color: "red" }} />
            </NavLink>
          </Fragment>
        );
      },
    },
  ];

  const onSearch = (value) => {};

  const deleteUser = (id) => {
    dispatch(deleteTaskByIdAction(id));
  };

  const onChange = (pagination, filters, sorter, extra) => {};

  return (
    <div className="container">
      <div>
        <button
          onClick={() => {
            window.location.href = "/admin/addTask";
          }}
          className="py-2 px-5 rounded-md bg-green-500 hover:bg-green-400 text-white font-medium"
        >
          Thêm Công việc
        </button>
      </div>

      <Search
        className="my-5"
        placeholder="input search text"
        enterButton={<SearchOutlined />}
        size="large"
        onSearch={onSearch}
        disabled
      />

      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}
