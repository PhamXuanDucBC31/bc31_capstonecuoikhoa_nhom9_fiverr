import React, { useEffect } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { Input, Table } from "antd";
import {
  deleteRentTaskAction,
  getRentTaskAction,
  postDoneTaskByIdAction,
} from "../../../redux/Actions/taskAction";

export default function ServiceManagement() {
  const dispatch = useDispatch();
  const { Search } = Input;
  const { arrRenttask } = useSelector((state) => state.taskReducer);
  useEffect(() => {
    dispatch(getRentTaskAction());
  }, []);

  const data = arrRenttask;

  const columns = [
    // render tài khoản
    {
      title: "id",
      dataIndex: "id",
      width: "5%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend", "ascend"],
    },

    // render mã công việc
    {
      title: "Mã Công việc",
      dataIndex: "maCongViec",
      width: "8%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend", "ascend"],
    },

    //render mã người thuê
    {
      title: "Mã người thuê",
      dataIndex: "maNguoiThue",
      width: "8%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend", "ascend"],
    },

    //rener ngày thuê
    {
      title: "Ngày thuê",
      dataIndex: "ngayThue",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sortDirections: ["descend", "ascend"],
    },

    {
      title: "Hoàn thành",
      dataIndex: "hoanThanh",
      width: "10%",
      //sort
      render: (text, arrRenttask, index) => {
        if (arrRenttask.hoanThanh == true) {
          return (
            <span className="text-green-500 font-bold">Đã hoàn thành</span>
          );
        } else {
          return (
            <span className="text-red-500 font-bold">Chưa hoàn thành</span>
          );
        }
      },
      sortDirections: ["descend", "ascend"],
    },

    //render thao tác
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      width: "10%",

      sortDirections: ["descend", "ascend"],
      render: (text, arrRenttask, index) => {
        if (arrRenttask.hoanThanh == false) {
          return (
            <button
              onClick={() => {
                doneTask(arrRenttask.id);
              }}
              className="bg-green-500 py-2 px-5 text-white font-medium rounded-md hover:bg-green-300"
            >
              Hoàn thành
            </button>
          );
        } else {
          return (
            <button
              onClick={() => {
                deleteRentTask(arrRenttask.id);
              }}
              className="bg-red-500 py-2 px-5 text-white font-medium rounded-md hover:bg-red-300"
            >
              Xoá
            </button>
          );
        }
      },
    },
  ];

  const onSearch = (value) => {};

  const deleteRentTask = (id) => {
    dispatch(deleteRentTaskAction(id));
  };

  const doneTask = (id) => {
    dispatch(postDoneTaskByIdAction(id));
  };

  const onChange = (pagination, filters, sorter, extra) => {};

  return (
    <div className="container">
      <Search
        className="my-5"
        placeholder="input search text"
        enterButton={<SearchOutlined />}
        size="large"
        onSearch={onSearch}
        disabled
      />

      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}
