import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Input, Table } from "antd";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import {
  deleteUserAction,
  getListOfUserInfoAction,
  getSearchUserByNameAction,
} from "../../../redux/Actions/userAction";

export default function UserManagement() {
  const dispatch = useDispatch();
  const { Search } = Input;
  const { userList } = useSelector((state) => state.userReducer);

  useEffect(() => {
    dispatch(getListOfUserInfoAction());
  }, []);

  const columns = [
    // render tài khoản
    {
      title: "Tài khoản",
      dataIndex: "id",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend", "ascend"],
    },

    // render họ tên
    {
      title: "Họ tên",
      dataIndex: "name",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => {
        let nameA = a.name.toLowerCase().trim();
        let nameB = b.name.toLowerCase().trim();
        if (nameA > nameB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    //render email
    {
      title: "E-mail",
      dataIndex: "email",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => {
        let emailA = a.email.toLowerCase().trim();
        let emailB = b.email.toLowerCase().trim();
        if (emailA > emailB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    //rener role người dùng
    {
      title: "Loại người dùng",
      dataIndex: "role",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sortDirections: ["descend", "ascend"],
    },

    //render thao tác
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      width: "10%",

      sortDirections: ["descend", "ascend"],
      render: (text, userList, index) => {
        return (
          <Fragment>
            <NavLink
              key={1}
              className="mr-2 text-2xl"
              to={`/admin/editUser/${userList.id}`}
            >
              <EditOutlined style={{ color: "blue" }} />
            </NavLink>

            <NavLink
              key={2}
              className="ml-2 text-2xl"
              onClick={() => {
                deleteUser(userList.id);
              }}
            >
              <DeleteOutlined style={{ color: "red" }} />
            </NavLink>
          </Fragment>
        );
      },
    },
  ];

  const onSearch = (value) => {
    dispatch(getSearchUserByNameAction(value));
  };
  const deleteUser = (id) => {
    dispatch(deleteUserAction(id));
  };
  const data = userList;

  const onChange = (pagination, filters, sorter, extra) => {};

  const addAdmin = () => {
    window.location.href = "/admin/addAdmin";
  };

  return (
    <div className="container">
      <div>
        <button
          onClick={() => {
            addAdmin();
          }}
          className="py-2 px-5 rounded-md bg-green-500 hover:bg-green-400 text-white font-medium"
        >
          Thêm Quản trị viên
        </button>
      </div>

      <Search
        className="my-5"
        placeholder="input search text"
        enterButton={<SearchOutlined />}
        size="large"
        onSearch={onSearch}
      />

      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}
