import React from "react";
import { Form, Input } from "antd";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import { addAdminAction } from "../../../../redux/Actions/userAction";

export default function AddAdmin() {
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: "",
      email: "",
      password: "",
      gender: true,
      role: "ADMIN",
      phone: "",
    },
    onSubmit: (values) => {
      console.log(values);

      dispatch(addAdminAction(values));
    },
  });
  return (
    <div>
      <h3 className="text-4xl mb-5">Thêm Quản trị viên</h3>

      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
      >
        <Form.Item label="Tên người dùng" onChange={formik.handleChange}>
          <Input value={formik.values.name} name="name" />
        </Form.Item>

        <Form.Item label="Mật khẩu" onChange={formik.handleChange}>
          <Input value={formik.values.password} name="password" />
        </Form.Item>

        <Form.Item label="E-mail" onChange={formik.handleChange}>
          <Input value={formik.values.email} name="email" />
        </Form.Item>

        <Form.Item label="Số điện thoại" onChange={formik.handleChange}>
          <Input type="number" value={formik.values.phone} name="phone" />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button
            type="submit"
            className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
          >
            THÊM QUẢN TRỊ VIÊN
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
