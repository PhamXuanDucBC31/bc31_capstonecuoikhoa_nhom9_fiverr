import React, { useEffect } from "react";
import { Form, Input, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import { useParams } from "react-router-dom";
import {
  getUserInFoByID,
  putUserByIdAction,
} from "../../../../redux/Actions/userAction";

export default function EditUser() {
  const dispatch = useDispatch();
  const { id } = useParams();
  useEffect(() => {
    dispatch(getUserInFoByID(id));
  }, []);

  const { userInfoByID } = useSelector((state) => state.userReducer);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      hoTen: userInfoByID.name,
      email: userInfoByID.email,
      phone: userInfoByID.phone,
      role: userInfoByID.role,
      gender: userInfoByID.gender,
      avatar: userInfoByID.avatar,
    },
    onSubmit: (values) => {
      console.log(values);

      dispatch(putUserByIdAction(userInfoByID.id, values));
    },
  });

  const handleChangeSelect = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  return (
    <div>
      <h3 className="text-4xl mb-5">Cập nhật người dùng</h3>

      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
      >
        <Form.Item label="Tên người dùng" onChange={formik.handleChange}>
          <Input value={formik.values.hoTen} name="hoTen" />
        </Form.Item>

        <Form.Item label="E-mail" onChange={formik.handleChange}>
          <Input value={formik.values.email} name="email" />
        </Form.Item>

        <Form.Item label="Số điện thoại" onChange={formik.handleChange}>
          <Input value={formik.values.phone} name="phone" />
        </Form.Item>

        <Form.Item label="Gender">
          <Select defaultValue="1" onChange={handleChangeSelect("gender")}>
            <Select.Option value="1">{formik.values.gender}</Select.Option>
            <Select.Option value={true}>True</Select.Option>
            <Select.Option value={false}>false</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Loại người dùng">
          <Select defaultValue="1" onChange={handleChangeSelect("role")}>
            <Select.Option value="1">{formik.values.role}</Select.Option>
            <Select.Option value="ADMIN">ADMIN</Select.Option>
            <Select.Option value="USER">USER</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Avatar" onChange={formik.handleChange}>
          <Input value={formik.values.avatar} name="avatar" />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button
            type="submit"
            className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
          >
            CẬP NHẬT NGƯỜI DÙNG
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
