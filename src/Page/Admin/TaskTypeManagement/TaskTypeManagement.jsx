import React, { Fragment, useEffect } from "react";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Input, Pagination, Table } from "antd";
import {
  deleteTypeOfTaskByIdAction,
  getTypeOfTaskManagementAction,
} from "../../../redux/Actions/taskAction";

export default function TaskTypeManagement() {
  const dispatch = useDispatch();
  const { Search } = Input;
  const { arrTypeOfTaskManageMent } = useSelector((state) => state.taskReducer);
  useEffect(() => {
    dispatch(getTypeOfTaskManagementAction());
  }, []);

  const data = arrTypeOfTaskManageMent;

  const columns = [
    // render tài khoản
    {
      title: "id",
      dataIndex: "id",
      width: "30%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend", "ascend"],
    },

    // render họ tên
    {
      title: "Tên loại công việc",
      dataIndex: "tenLoaiCongViec",
      width: "30%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => {
        let tenLoaiCongViecA = a.tenLoaiCongViec.toLowerCase().trim();
        let tenLoaiCongViecB = b.tenLoaiCongViec.toLowerCase().trim();
        if (tenLoaiCongViecA > tenLoaiCongViecB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    //render thao tác
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      width: "30%",

      sortDirections: ["descend", "ascend"],
      render: (text, arrTypeOfTaskManageMent, index) => {
        return (
          <Fragment>
            <NavLink
              to={`/admin/editTaskType/${arrTypeOfTaskManageMent.id}`}
              key={1}
              className="mr-2 text-2xl"
            >
              <EditOutlined style={{ color: "blue" }} />
            </NavLink>

            <NavLink
              key={2}
              className="ml-2 text-2xl"
              onClick={() => {
                deleteType(arrTypeOfTaskManageMent.id);
              }}
            >
              <DeleteOutlined style={{ color: "red" }} />
            </NavLink>
          </Fragment>
        );
      },
    },
  ];

  const onSearch = (value) => {};
  const deleteType = (id) => {
    dispatch(deleteTypeOfTaskByIdAction(id));
  };
  const onChange = (pagination, filters, sorter, extra) => {};

  return (
    <div className="container">
      <div>
        <button
          onClick={() => {
            window.location.href = "/admin/addTaskType";
          }}
          className="py-2 px-5 rounded-md bg-green-500 hover:bg-green-400 text-white font-medium"
        >
          Thêm loại công việc
        </button>
      </div>{" "}
      <Search
        className="my-5"
        placeholder="Tìm kiếm loại công việc theo ID (không có api phù hợp)"
        enterButton={<SearchOutlined />}
        size="large"
        disabled
        onSearch={onSearch}
      />
      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}
