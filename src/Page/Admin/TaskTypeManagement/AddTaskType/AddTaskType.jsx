import { useFormik } from "formik";
import React from "react";
import { Form, Input } from "antd";
import { useDispatch } from "react-redux";
import { postAddTaskTypeAction } from "../../../../redux/Actions/taskAction";

export default function AddTaskType() {
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: 0,
      tenLoaiCongViec: "",
    },
    onSubmit: (values) => {
      dispatch(postAddTaskTypeAction(values));
    },
  });

  return (
    <div className="container">
      <h3 className="text-4xl mb-5">Thêm Loại Công việc</h3>

      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
      >
        <Form.Item label="Tên loại công việc" onChange={formik.handleChange}>
          <Input value={formik.values.tenLoaiCongViec} name="tenLoaiCongViec" />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button
            type="submit"
            className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
          >
            THÊM CÔNG VIỆC
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
