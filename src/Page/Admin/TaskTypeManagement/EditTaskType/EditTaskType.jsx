import { useFormik } from "formik";
import React, { useEffect } from "react";
import { Form, Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  getTypeOfTaskInfoByIdAction,
  putUpdatedTypeOfTaskAction,
} from "../../../../redux/Actions/taskAction";
import { useParams } from "react-router-dom";

export default function EditTaskType() {
  const dispatch = useDispatch();

  const { id } = useParams();

  useEffect(() => {
    dispatch(getTypeOfTaskInfoByIdAction(id));
  }, []);

  const { typeOfTaskSelected } = useSelector((state) => state.taskReducer);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: typeOfTaskSelected.id,
      tenLoaiCongViec: typeOfTaskSelected.tenLoaiCongViec,
    },
    onSubmit: (values) => {
      dispatch(putUpdatedTypeOfTaskAction(id, values));
    },
  });
  return (
    <div className="container">
      <h3 className="text-4xl mb-5">Chỉnh sửa Loại Công việc</h3>

      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
      >
        <Form.Item label="ID" onChange={formik.handleChange}>
          <Input disabled value={formik.values.id} name="id" />
        </Form.Item>

        <Form.Item label="Tên loại công việc" onChange={formik.handleChange}>
          <Input value={formik.values.tenLoaiCongViec} name="tenLoaiCongViec" />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button
            type="submit"
            className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
          >
            Cập nhật loại công việc
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
