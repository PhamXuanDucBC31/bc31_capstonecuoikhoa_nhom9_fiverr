import { message, Tabs } from "antd";
import moment from "moment";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { rentTaskAction } from "../../../redux/Actions/taskAction";
import { localStorageServ } from "../../../Service/localStorageService";

export default function RentTask(props) {
  let id = props.id;
  const dispatch = useDispatch();
  const { arrTask } = useSelector((state) => state.taskReducer);

  const onChange = (key) => {
    console.log(key);
  };

  if (arrTask.length != 0) {
    const task = arrTask.filter((task) => {
      return task.id == id;
    });
    const { congViec } = {
      ...task[0],
    };

    const { moTaNgan, giaTien } = {
      ...congViec,
    };

    const thueCongViec = () => {
      if (localStorageServ.user.get() === null) {
        message.warning("vui lòng đăng nhập trước khi bình luận");

        setTimeout(() => {
          window.location.href = "/login";
        }, 1000);
      } else {
        const dataThueCongViec = {
          id: moment,
          maCongViec: id,
          maNguoiThue: localStorageServ.user.get().user.id,
          ngayThue: moment().format("DD/MM/YYYY"),
          hoanThanh: false,
        };

        dispatch(rentTaskAction(dataThueCongViec));
      }
    };

    return (
      <div>
        <Tabs
          className="justify-between"
          centered={true}
          size={"large"}
          defaultActiveKey="1"
        >
          <Tabs.TabPane tab="Basic" key="1">
            <div className="flex text-xl font-medium justify-between">
              <h1>Basic</h1>

              <p>${giaTien}</p>
            </div>

            <p
              className="font-medium my-5  text-gray-500"
              style={{ whiteSpace: "pre-line" }}
            >
              {moTaNgan}
            </p>

            <div className="flex justify-center">
              <button
                onClick={() => {
                  thueCongViec();
                }}
                className="px-32 rounded-md text-white font-semibold hover:bg-green-400 py-3 bg-green-500"
              >
                Continue (${giaTien})
              </button>
            </div>
          </Tabs.TabPane>

          <Tabs.TabPane tab="Standard" key="2">
            <div className="flex text-xl font-medium justify-between">
              <h1>Standard</h1>

              <p>${giaTien * 1.5}</p>
            </div>

            <p
              className="font-medium my-5  text-gray-500"
              style={{ whiteSpace: "pre-line" }}
            >
              {moTaNgan}
            </p>

            <div className="flex justify-center">
              <button
                onClick={() => {
                  thueCongViec();
                }}
                className="px-32 rounded-md text-white font-semibold hover:bg-green-400 py-3 bg-green-500"
              >
                Continue (${giaTien * 1.5})
              </button>
            </div>
          </Tabs.TabPane>

          <Tabs.TabPane tab="Premium" key="3">
            <div className="flex text-xl font-medium justify-between">
              <h1>Premium</h1>

              <p>${giaTien * 2}</p>
            </div>

            <p
              className="font-medium my-5  text-gray-500"
              style={{ whiteSpace: "pre-line" }}
            >
              {moTaNgan}
            </p>

            <div className="flex justify-center">
              <button
                onClick={() => {
                  thueCongViec();
                }}
                className="px-32 rounded-md text-white font-semibold hover:bg-green-400 py-3 bg-green-500"
              >
                Continue (${giaTien * 2})
              </button>
            </div>
          </Tabs.TabPane>
        </Tabs>
      </div>
    );
  }
}
