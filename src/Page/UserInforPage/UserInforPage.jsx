import { Rate } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTaskRentedAction } from "../../redux/Actions/taskAction";
import "./UserInforPage.css";

export default function UserInforPage() {
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.userReducer);

  useEffect(() => {
    dispatch(getTaskRentedAction());
  }, []);

  const { arrTaskRented } = useSelector((state) => state.taskReducer);
  const { hinhAnh } = { ...arrTaskRented.congViec };

  const updateUserInfo = () => {
    console.log(hinhAnh);
  };

  const renderTaskRented = () => {
    return arrTaskRented.map((item) => {
      return (
        <div className="tastRented_content grid grid-cols-4 gap-2 my-4">
          <img className="col-span-1 " src={item.congViec.hinhAnh} alt="" />

          <div className="rentContent col-span-3">
            <p className="text-lg mb-2">{item.congViec.tenCongViec}</p>

            <div className="rating">
              <Rate
                className="pb-1"
                disabled
                value={item.congViec.saoCongViec}
              />
            </div>

            <span className="font-medium">{item.congViec.moTaNgan}</span>
          </div>
        </div>
      );
    });
  };

  const { avatar, name, certification, skill } = { ...userInfo.user };

  return (
    <div
      className="px-auto grid grid-cols-3 gap-10"
      style={{ backgroundColor: "#f7f7f7", padding: "40px" }}
    >
      <div className="info-column col-span-1 p-5 flex-col">
        <div className="avatar mx-auto">
          <img src={avatar} alt="" />
        </div>

        <div className="name mx-auto mt-5">{name}</div>

        <div className="editInfo mx-auto mt-5">
          <button
            className="text-blue-500 hover:font-medium hover:text-blue-800"
            onClick={() => {
              updateUserInfo();
            }}
          >
            Cập nhật thông tin
          </button>
        </div>

        <h1 className="text-xl mt-10 font-medium text-green-600">
          Description
        </h1>
        <p className="text-green-600 font-medium text-lg m-5">
          Certification: {certification}
        </p>

        <p className="text-green-600  font-medium text-lg m-5">
          Skill: {skill}
        </p>
      </div>

      <div className="active-column p-5 col-span-2">
        <p className="text-xl font-medium text-green-600">Công việc đã thuê</p>
        {renderTaskRented()}
      </div>
    </div>
  );
}
