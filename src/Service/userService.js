import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";

export let userService = {
  postLogin: (dataLogin) => {
    return https.post("/api/auth/signin", dataLogin);
  },
  postRegister: (dataRegister) => {
    return https.post("/api/auth/signup", dataRegister);
  },

  getListOfUserInfo: () => {
    return https.get("/api/users");
  },

  deleteUser: (id) => {
    return https.delete(`/api/users?id=${id}`);
  },

  getUserInFoByID: (id) => {
    return https.get(`/api/users/${id}`);
  },

  putUserByID: (id, dataPut) => {
    return axios({
      url: `https://fiverrnew.cybersoft.edu.vn/api/users/${id}`,
      method: "PUT",
      data: dataPut,
      headers: {
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  getSearchUserByName: (tenNguoiDung) => {
    return https.get(`/api/users/search/${tenNguoiDung}`);
  },

  addAdmin: (dataAdd) => {
    return https.post("/api/users", dataAdd);
  },
};
