import { data } from "autoprefixer";
import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
import { localStorageServ } from "./localStorageService";

export const taskService = {
  getTaskListByName: (tenCongViec) => {
    return https.get(
      `/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${tenCongViec}`
    );
  },

  getTaskListByType: (maChiTietLoai) => {
    return https.get(
      `/api/cong-viec/lay-cong-viec-theo-chi-tiet-loai/${maChiTietLoai}`
    );
  },

  getTypeOfTask: () => {
    return https.get(`/api/cong-viec/lay-menu-loai-cong-viec`);
  },

  getDetailTypeOfTask: (maLoaiCongViec) => {
    return https.get(
      `/api/cong-viec/lay-chi-tiet-loai-cong-viec/${maLoaiCongViec}`
    );
  },

  getTaskSelected: (maCongViec) => {
    return https.get(`/api/cong-viec/lay-cong-viec-chi-tiet/${maCongViec}`);
  },

  getCommentByTask: (maCongViec) => {
    return https.get(
      `/api/binh-luan/lay-binh-luan-theo-cong-viec/${maCongViec}`
    );
  },

  postComment: (dataComment) => {
    return axios({
      url: "https://fiverrnew.cybersoft.edu.vn/api/binh-luan",
      method: "POST",
      data: dataComment,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  rentTask: (dataThueCongViec) => {
    return axios({
      url: "https://fiverrnew.cybersoft.edu.vn/api/thue-cong-viec",
      method: "POST",
      data: dataThueCongViec,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  getTaskList: () => {
    return https.get("/api/cong-viec");
  },

  getTypeOfTaskManagement: () => {
    return https.get("/api/loai-cong-viec");
  },

  getRentTask: () => {
    return https.get("/api/thue-cong-viec");
  },

  postAddTask: (dataAdd) => {
    return axios({
      url: "https://fiverrnew.cybersoft.edu.vn/api/cong-viec",
      method: "POST",
      data: dataAdd,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  deleteTaskById: (id) => {
    return axios({
      url: `https://fiverrnew.cybersoft.edu.vn/api/cong-viec/${id}`,
      method: "DELETE",
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  postAddTaskType: (dataAdd) => {
    return axios({
      url: "https://fiverrnew.cybersoft.edu.vn/api/loai-cong-viec",
      method: "POST",
      data: dataAdd,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  deleteTypeOfTaskByID: (id) => {
    return axios({
      url: `https://fiverrnew.cybersoft.edu.vn/api/loai-cong-viec/${id}`,
      method: "DELETE",
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  getTypeOfTaskInfoById: (id) => {
    return https.get(`/api/loai-cong-viec/${id}`);
  },

  putUpdatedTypeOfTask: (id, dataPut) => {
    return axios({
      url: `https://fiverrnew.cybersoft.edu.vn/api/loai-cong-viec/${id}`,
      method: "PUT",
      data: dataPut,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  getTaskUpdateInfoById: (id) => {
    return https.get(`/api/cong-viec/${id}`);
  },

  putTaskUpdated: (id, dataPut) => {
    return axios({
      url: `https://fiverrnew.cybersoft.edu.vn/api/cong-viec/${id}`,
      method: "PUT",
      data: dataPut,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  postDoneTaskById: (id) => {
    return https.post(`/api/thue-cong-viec/hoan-thanh-cong-viec/${id}`);
  },

  deleteRentTask: (id) => {
    return axios({
      url: `https://fiverrnew.cybersoft.edu.vn/api/thue-cong-viec/${id}`,
      method: "DELETE",
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  getTaskRented: () => {
    return axios({
      url: `https://fiverrnew.cybersoft.edu.vn/api/thue-cong-viec/lay-danh-sach-da-thue`,
      method: "GET",
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },
};
