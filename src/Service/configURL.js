import axios from "axios";
export const TOKEN_CYBER =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJEYXRhIEFuYWx5c3RzIDA4IiwiSGV0SGFuU3RyaW5nIjoiMDMvMTAvMjAyMyIsIkhldEhhblRpbWUiOiIxNjk2MjkxMjAwMDAwIiwibmJmIjoxNjc1MzU3MjAwLCJleHAiOjE2OTY0Mzg4MDB9.8HV9TGOCddLippI21Eoc2lwrPQ6pj3scfu8u-8i5TaQ";

export let https = axios.create({
  baseURL: "https://fiverrnew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: TOKEN_CYBER,
  },
});

// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
