import React, { useState } from "react";
import {
  ContainerOutlined,
  TableOutlined,
  TeamOutlined,
  ToolOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout, Menu, message } from "antd";
import { NavLink } from "react-router-dom";
import { localStorageServ } from "../Service/localStorageService";

const { Content, Footer, Sider } = Layout;

export default function AdminLayout({ Component }) {
  let user = localStorageServ.user.get();

  let isAdmin =
    localStorageServ.user.get()?.user.role === "ADMIN" ? true : false;

  if (isAdmin == true) {
    return (
      <Layout
        style={{
          minHeight: "100vh",
        }}
      >
        <Sider>
          <div
            style={{ height: "50px" }}
            className="logo flex justify-center items-center gap-2"
          >
            <UserOutlined style={{ color: "white" }} />

            <h1 className="text-white font-medium">{user.user.name} (ADMIN)</h1>
          </div>
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
            <Menu.Item key="1" icon={<TeamOutlined />}>
              <NavLink to="/admin">Users management</NavLink>
            </Menu.Item>
            <Menu.Item key="2" icon={<TableOutlined />}>
              <NavLink to="/admin/taskType">Task Type management</NavLink>
            </Menu.Item>
            <Menu.Item key="3" icon={<ContainerOutlined />}>
              <NavLink to="/admin/task">Task management</NavLink>
            </Menu.Item>
            <Menu.Item key="4" icon={<ToolOutlined />}>
              <NavLink to="/admin/service">Service management</NavLink>
            </Menu.Item>
          </Menu>
        </Sider>

        <Layout className="site-layout">
          <Content
            style={{
              margin: "0 16px",
            }}
          >
            <div
              className="site-layout-background"
              style={{
                padding: 24,
                minHeight: 360,
              }}
            >
              <Component />
            </div>
          </Content>

          <Footer
            style={{
              textAlign: "center",
            }}
          >
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    );
  } else {
    message.warning("VUI LÒNG ĐĂNG NHẬP TÀI KHOẢN ADMIN");

    setTimeout(() => {
      window.location.href = "/login";
    }, 1500);
  }
}
