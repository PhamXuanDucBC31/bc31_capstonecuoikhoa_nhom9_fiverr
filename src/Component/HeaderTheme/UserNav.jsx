import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getTypeOfTaskAction } from "../../redux/Actions/taskAction";

import "./UserNav.css";
export default function UserNav() {
  let dispatch = useDispatch();
  const { arrTypeOfTask } = useSelector((state) => state.taskReducer);
  useEffect(() => {
    dispatch(getTypeOfTaskAction());
  }, []);

  const changeToDetailTypeOfTask = (value) => {
    window.location.href = `/typeOfTask/${value}`;
  };
  const changeToDetailListOfTask = (value) => {
    window.location.href = `/taskList/categories/${value}`;
  };

  let renderTypeOfTask = () => {
    return arrTypeOfTask.map((type) => {
      return (
        <div className="dropdown" key={type.id}>
          <p
            className="dropdown-title mx-4"
            onClick={() => {
              changeToDetailTypeOfTask(type.id);
            }}
          >
            {type.tenLoaiCongViec}
          </p>
          <div className="dropdown-content p-5">
            {type.dsNhomChiTietLoai.map((item) => {
              return (
                <ul key={item.id}>
                  <li>
                    <h1 className="font-bold">{item.tenNhom}</h1>
                  </li>
                  {item.dsChiTietLoai.map((item2) => {
                    return (
                      <li key={item2.id}>
                        <a
                          href="#"
                          onClick={() => {
                            changeToDetailListOfTask(item2.id);
                          }}
                        >
                          {item2.tenChiTiet}
                        </a>
                      </li>
                    );
                  })}
                </ul>
              );
            })}
          </div>
        </div>
      );
    });
  };

  return (
    <div className="menu-navbar ">
      <ul className="flex items-center justify-center">
        <li>{renderTypeOfTask()}</li>
      </ul>
    </div>
  );
}
