import { message } from "antd";
import { userService } from "../../Service/userService";
import {
  GET_DANH_SACH_NGUOI_DUNG,
  GET_SEARCH_USER_BY_NAME,
  GET_USER_INFO_BY_ID,
  LOGIN,
  REGISTER,
} from "../Constants/userConstant";

export let loginAction = (dataLogin) => {
  return {
    type: LOGIN,
    payload: dataLogin,
  };
};

export let registerAction = (dataRegister) => {
  return {
    type: REGISTER,
    payload: dataRegister,
  };
};

export const getListOfUserInfoAction = () => {
  return (dispatch) => {
    userService
      .getListOfUserInfo()
      .then((res) => {
        dispatch({
          type: GET_DANH_SACH_NGUOI_DUNG,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const deleteUserAction = (id) => {
  return (dispatch) => {
    userService
      .deleteUser(id)
      .then((res) => {
        message.success(res.data.message);

        setTimeout(() => {
          dispatch(getListOfUserInfoAction());
        }, 1000);
      })
      .catch((err) => {
        message.warning(err.response.data);
      });
  };
};

export const getUserInFoByID = (id) => {
  return (dispatch) => {
    userService
      .getUserInFoByID(id)
      .then((res) => {
        dispatch({
          type: GET_USER_INFO_BY_ID,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const putUserByIdAction = (id, dataPut) => {
  return (dispatch) => {
    userService
      .putUserByID(id, dataPut)
      .then((res) => {
        message.success("Cập nhật thành công");

        setTimeout(() => {
          window.location.href = "/admin";
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const getSearchUserByNameAction = (tenNguoiDung) => {
  return (dispatch) => {
    userService
      .getSearchUserByName(tenNguoiDung)
      .then((res) => {
        dispatch({
          type: GET_SEARCH_USER_BY_NAME,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        message.warning("Không tìm thấy người dùng");
      });
  };
};

export const addAdminAction = (addAdmin) => {
  return (dispatch) => {
    userService
      .addAdmin(addAdmin)
      .then((res) => {
        message.success("Thêm quản trị viên thành công");

        setTimeout(() => {
          window.location.href = "/admin";
        }, 1000);
      })
      .catch((err) => {});
  };
};
