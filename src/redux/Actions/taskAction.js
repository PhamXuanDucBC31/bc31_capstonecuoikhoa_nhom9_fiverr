import { message } from "antd";
import { taskService } from "../../Service/taskService";
import {
  GET_COMMENT_BY_TASK,
  GET_DETAIL_TYPE_OF_TASK,
  GET_RENT_TASK,
  GET_TASK_LISK,
  GET_TASK_LIST_BY_NAME,
  GET_TASK_LIST_BY_TYPE,
  GET_TASK_RENTED,
  GET_TASK_SELECTED,
  GET_TASK_UPDATE_INFO_BY_ID,
  GET_TYPE_OF_TASK,
  GET_TYPE_OF_TASK_INFO_BY_ID,
  GET_TYPE_OF_TASK_MANAGEMENT,
} from "../Constants/taskConstant";

export const getTaskListByNameAction = (tenCongViec) => {
  return (dispatch) => {
    taskService
      .getTaskListByName(tenCongViec)
      .then((res) => {
        dispatch({
          type: GET_TASK_LIST_BY_NAME,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getTaskListByTypeAction = (maChiTietLoai) => {
  return (dispatch) => {
    taskService
      .getTaskListByType(maChiTietLoai)
      .then((res) => {
        dispatch({
          type: GET_TASK_LIST_BY_TYPE,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const getTypeOfTaskAction = () => {
  return (dispatch) => {
    taskService
      .getTypeOfTask()
      .then((res) => {
        dispatch({
          type: GET_TYPE_OF_TASK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getDetailTypeOfTaskAction = (maLoaiCongViec) => {
  return (dispatch) => {
    taskService
      .getDetailTypeOfTask(maLoaiCongViec)
      .then((res) => {
        dispatch({
          type: GET_DETAIL_TYPE_OF_TASK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getTaskSelectedAction = (maCongViec) => {
  return (dispatch) => {
    taskService
      .getTaskSelected(maCongViec)
      .then((res) => {
        dispatch({
          type: GET_TASK_SELECTED,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getCommentByTaskAction = (maCongViec) => {
  return (dispatch) => {
    taskService
      .getCommentByTask(maCongViec)
      .then((res) => {
        dispatch({
          type: GET_COMMENT_BY_TASK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const postCommentAction = (dataComment) => {
  return (dispatch) => {
    taskService
      .postComment(dataComment)
      .then((res) => {
        message.success("Gửi bình luận thành công");
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const rentTaskAction = (dataThueCongViec) => {
  return (dispatch) => {
    taskService
      .rentTask(dataThueCongViec)
      .then((res) => {
        message.success(res.data.message);
      })
      .catch((err) => {});
  };
};

export const gettaskListAction = () => {
  return (dispatch) => {
    taskService
      .getTaskList()
      .then((res) => {
        dispatch({
          type: GET_TASK_LISK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getTypeOfTaskManagementAction = () => {
  return (dispatch) => {
    taskService
      .getTypeOfTaskManagement()
      .then((res) => {
        dispatch({
          type: GET_TYPE_OF_TASK_MANAGEMENT,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getRentTaskAction = () => {
  return (dispatch) => {
    taskService
      .getRentTask()
      .then((res) => {
        dispatch({
          type: GET_RENT_TASK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const postAddTaskAction = (dataAdd) => {
  return (dispatch) => {
    taskService
      .postAddTask(dataAdd)
      .then((res) => {
        message.success(res.data.message);

        setTimeout(() => {
          window.location.href = "/admin/task";
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const deleteTaskByIdAction = (id) => {
  return (dispatch) => {
    taskService
      .deleteTaskById(id)
      .then((res) => {
        message.success(res.data.message);

        setTimeout(() => {
          dispatch(gettaskListAction());
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const postAddTaskTypeAction = (dataAdd) => {
  return (dispatch) => {
    taskService
      .postAddTaskType(dataAdd)
      .then((res) => {
        message.success(res.data.message);

        setTimeout(() => {
          window.location.href = "/admin/taskType";
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const deleteTypeOfTaskByIdAction = (id) => {
  return (dispatch) => {
    taskService
      .deleteTypeOfTaskByID(id)
      .then((res) => {
        message.success(res.data.message);

        setTimeout(() => {
          dispatch(getTypeOfTaskManagementAction());
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const getTypeOfTaskInfoByIdAction = (id) => {
  return (dispatch) => {
    taskService
      .getTypeOfTaskInfoById(id)
      .then((res) => {
        dispatch({
          type: GET_TYPE_OF_TASK_INFO_BY_ID,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const putUpdatedTypeOfTaskAction = (id, dataPut) => {
  return (dispatch) => {
    taskService
      .putUpdatedTypeOfTask(id, dataPut)
      .then((res) => {
        message.success(res.data.message);

        setTimeout(() => {
          window.location.href = "/admin/taskType";
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const getTaskUpdateInfoByIdAction = (id) => {
  return (dispatch) => {
    taskService
      .getTaskUpdateInfoById(id)
      .then((res) => {
        dispatch({
          type: GET_TASK_UPDATE_INFO_BY_ID,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const putTaskUpdatedAction = (id, dataPut) => {
  return (dispatch) => {
    taskService
      .putTaskUpdated(id, dataPut)
      .then((res) => {
        message.success(res.data.message);

        setTimeout(() => {
          window.location.href = "/admin/task";
        }, 1000);
      })
      .catch((err) => {
        message.warning(err.response.data.content);
      });
  };
};

export const postDoneTaskByIdAction = (id) => {
  return (dispatch) => {
    taskService
      .postDoneTaskById(id)
      .then((res) => {
        message.success("Hoàn thành công việc");

        setTimeout(() => {
          dispatch(getRentTaskAction());
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const deleteRentTaskAction = (id) => {
  return (dispatch) => {
    taskService
      .deleteRentTask(id)
      .then((res) => {
        message.success("Xoá thành công");

        setTimeout(() => {
          dispatch(getRentTaskAction());
        }, 1000);
      })
      .catch((err) => {});
  };
};

export const getTaskRentedAction = () => {
  return (dispatch) => {
    taskService
      .getTaskRented()
      .then((res) => {
        dispatch({
          type: GET_TASK_RENTED,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
