import {
  GET_COMMENT_BY_TASK,
  GET_DETAIL_TYPE_OF_TASK,
  GET_RENT_TASK,
  GET_TASK_LISK,
  GET_TASK_LIST_BY_NAME,
  GET_TASK_LIST_BY_TYPE,
  GET_TASK_RENTED,
  GET_TASK_SELECTED,
  GET_TASK_UPDATE_INFO_BY_ID,
  GET_TYPE_OF_TASK,
  GET_TYPE_OF_TASK_INFO_BY_ID,
  GET_TYPE_OF_TASK_MANAGEMENT,
} from "../Constants/taskConstant";

const initialState = {
  arrTask: [],
  arrTypeOfTask: [],
  typeOfTask: "",
  arrDetailTypeOfTask: [],
  taskSelected: [],
  arrCommentByTask: [],
  arrTaskRented: [],
  // admidn pageeeee

  arrTaskList: [],
  arrTypeOfTaskManageMent: [],
  arrRenttask: [],
  typeOfTaskSelected: {},
  arrTaskUpdateInFo: {},
};

export const taskReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_TASK_LIST_BY_NAME: {
      state.arrTask = payload;
      return { ...state };
    }
    case GET_TASK_LIST_BY_TYPE: {
      state.arrTask = payload;
      state.typeOfTask = payload[0].tenChiTietLoai;
      return { ...state };
    }
    case GET_TYPE_OF_TASK: {
      state.arrTypeOfTask = payload;
      return { ...state };
    }

    case GET_DETAIL_TYPE_OF_TASK: {
      state.arrDetailTypeOfTask = payload;
      return { ...state };
    }

    case GET_TASK_SELECTED: {
      state.taskSelected = payload;
      return { ...state };
    }

    case GET_COMMENT_BY_TASK: {
      state.arrCommentByTask = payload;
      return { ...state };
    }

    // admim  pageeeeeeeeeeeeeeeeeeeeeee

    case GET_TASK_LISK: {
      state.arrTaskList = payload;
      return { ...state };
    }

    case GET_TYPE_OF_TASK_MANAGEMENT: {
      state.arrTypeOfTaskManageMent = payload;
      return { ...state };
    }

    case GET_RENT_TASK: {
      state.arrRenttask = payload;
      return { ...state };
    }

    case GET_TYPE_OF_TASK_INFO_BY_ID: {
      state.typeOfTaskSelected = payload;
      return { ...state };
    }

    case GET_TASK_UPDATE_INFO_BY_ID: {
      state.arrTaskUpdateInFo = payload;
      return { ...state };
    }

    case GET_TASK_RENTED: {
      state.arrTaskRented = payload;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
