import { localStorageServ } from "../../Service/localStorageService";
import {
  GET_DANH_SACH_NGUOI_DUNG,
  GET_SEARCH_USER_BY_NAME,
  GET_USER_INFO_BY_ID,
  LOGIN,
} from "../Constants/userConstant";

let initialState = {
  userInfo: localStorageServ.user.get(),
  userList: [],
  userInfoByID: {},
};

export let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN: {
      state.userInfo = action.payload;
      return { ...state };
    }

    case GET_DANH_SACH_NGUOI_DUNG: {
      state.userList = action.payload;
      return { ...state };
    }

    case GET_USER_INFO_BY_ID: {
      state.userInfoByID = action.payload;
      return { ...state };
    }

    case GET_SEARCH_USER_BY_NAME: {
      state.userList = action.payload;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
